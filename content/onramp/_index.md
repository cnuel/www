---
title: "OSPO OnRamp"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

{{< grid/div isMarkdown="false" >}}
<img src="/images/OspoOnRamp.png" alt="OSPO OnRamp" style="float: right; margin: 30px">
{{</ grid/div >}}

The OSPO OnRamp meeting series provides a low-threshold entry point for organisations that want to exchange and learn about the basics on how to set up an Open Source Program Office and get started into open source.

The 90 minute meeting is planned in a monthly cadence and will mainly consist of two parts.

In the first part an invited presenter will share experiences, lessons learned and other cool stuff. We plan to record this part of the meeting and to upload the recording later at the OSPO OnRamp website for later reference. This way we hope to gather and retain valuable information for the community.

In the second part of the meeting we would like to provide a trustful and protected environment where all participants can openly share and discuss their challenges, problems or other actual topics around establishing Open Source in their respective organisations. This part of the meeting will be held according to the [Chatham House Rules](https://en.wikipedia.org/wiki/Chatham_House_Rule#:~:text=When%20a%20meeting%2C%20or%20part,other%20participant%2C%20may%20be%20revealed.). Therefore there will be no recording of this part of the meeting.


## Next meetings

Meeting will be scheduled for every third Friday of the month from 10:30-12:00.

We provide an [ICS calendar file](/onramp/OSPO-OnRamp.ics) to easily import the meeting dates in any agenda.

* **Date**: Friday, September 16th, 10:30-12:00 CET \
  **Speakers**: Camille Moulin and Benjamin Jean ([InnoCube](https://innocube.org/)). \
  **Agenda**: Hermine project: Open source code and data for legal compliance \
  [Hermine](https://gitlab.com/hermine-project/hermine) is an Open Source community-driven project to manage SBOMs and the obligations that come from the licences they involve. It has been initiated by Inno³ and 5 partner companies: Enedis, Lectra, Orange, OVHCloud, RTE. \
The goal of the project is to share the code, but also the data, one very important aspect being to help standardise licence interpretations. Technically, it's a Django/Django REST project. It's still in early development stage, but the code is already publicly available.


Link to meeting: https://meet.jit.si/ospo.onramp


## Previous meetings

You can find all available recordings and available content from the previous meetings [here](/onramp/past_meetings).


## Mailing list

All news, infos and updates will be shared on the OSPO OnRamp mailing list.

Please subscribe at <https://framalistes.org/sympa/info/ospo.onramp>.


## Meeting topics

We would love to see topic proposals for the meeting via the maillinglist. The following list can be seen as starting point, which we will extend based on the community input:

* How to select the "right" open source project for adoption and contributions.
* How to identity sustainable, secure open source projects with respect also to processes and documentation.
* How to attract/raise interest of the C-Level management for a dedicated Open Source strategy.
* How to ease the formal "entry barriers" for developers to join open source activities.
* How to engage with the Open Source community as a company.


We don’t plan to establish a formal voting process but confirming interest into proposed projects via the mailing list will help us to identify the most relevant topics for the community.

Initially we will reach out to the OSPO Zone network to identify speakers, who are willing to share insights from their perspective, but mid- to long-term we hope that the community itself will identify the right speakers for upcoming meetings.


## Additional Resources

Resources coming from different Open Source and/or OSPO communities and Open Source professionals to help people advance in their open source journey.

Guides and books:

* [OSS Good Governance Handbook](https://ospo.zone/ggi/)
* [Open Source Program Offices: The Primer on Organizational Structures, Roles and Responsibilities, and Challenges](https://www.linkedin.com/pulse/open-source-program-offices-primer-organizational-roles-haddad/)
