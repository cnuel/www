---
title: "ospo.zone"
description: "The OSPO Alliance was launched in June 2021 by European non profit organisations — OW2, Eclipse Foundation, OpenForum Europe, and Foundation for Public Code — and concerned individuals to promote an approach to excellence in open source software management. Together we created the OSPO.Zone — an open experience-sharing platform to facilitate discovery of tools and best practices and help define the state of the art in this domain."
date: 2021-04-20T10:00:00-04:00
container: "home-container"
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
show_featured_story: false
show_featured_footer: false
headline: Welcome to the <span>OSPO.Zone!</span>
tagline: The OSPO Alliance’s platform for sharing and promoting world class materials on good governance for Open Source Program Offices.
layout: "single"
jumbotron_class: col-sm-14 col-md-12 col-lg-10 jumbotron-content
custom_jumbotron_class: jumbotron-image
jumbotron_tagline_class: jumbotron-tagline
links: [[href: "/position-paper", text: "Learn More"],[href: "/contribute", text: "Join us!"]]

custom_jumbotron: <img src="images/icons/icon-jumbotron.png" alt="jumbotron-icon" >
---

{{< home/section-discover >}}
{{< home/section-news >}}
  {{< newsroom/news id="news-pub-target" publishTarget="ospo_zone">}}
{{< /home/section-news >}}
{{< home/section-document-tools >}}
{{< home/section-help >}}
{{< home/section-launched-by >}}
